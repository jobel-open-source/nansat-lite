# nansat-lite
Inspired by the [Scientist friendly Python toolbox for processing 2D satellite Earth observation data](https://github.com/nansencenter/nansat/wiki).

Original nansat repository can be found in [Github](https://github.com/nansencenter/nansat).

# Motivation.

Required a python 3.5 implementation. 

# nansat-lite, what pieces are included?

nansat-lite is not a full nansat build for python 3.5. Only bits of code from main classes, to start with. Eventually, if need it, more code will be added.


