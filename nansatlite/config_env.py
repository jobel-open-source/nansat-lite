# -*- coding:utf-8 -*-
# Use this file to configure the environment variables
import os
# This file contains the run-time environmental variables
GDAL_DATA = r'/home/jobel/anaconda3/share/gdal'
GEOS_DIR = r'/home/jobel/anaconda3'

# GDAL needs an environment variable `GDAL_DATA` to find and use projection info.
# If it is not available, then some things stop working.
# The SRID look-up codes are in `GDAL_DATA`, for instance.

# 'GDAL_PATH' in os.environ
# if False, it should be added to either your system's environment variables,
# Here is added in run-time:
if 'GDAL_DATA' not in os.environ:
    os.environ['GDAL_DATA'] = GDAL_DATA
if 'GEOS_DIR' not in os.environ:
    os.environ['GEOS_DIR'] = GEOS_DIR

# To use pixexlfunctions in nansat or nansatlite it is required to:
# export GDAL_DRIVER_PATH=<PATH_TO_GDAL_PIXFUN_SO>:$GDAL_DRIVER_PATH

GDAL_DRIVER_PATH = r'/home/jobel/git_repos/aquabiota_solutions/nansatlite/nansatlite/pixelfunctions'
if 'GDAL_DRIVER_PATH' not in os.environ:
    os.environ['GDAL_DRIVER_PATH'] = GDAL_DRIVER_PATH

# Sanity check
is_GDAL_DATA_set = ('GDAL_DATA' in os.environ)
is_GEOS_DIR_set = ('GEOS_DIR' in os.environ)
is_GDAL_DRIVER_PATH = ('GDAL_DRIVER_PATH' in os.environ)



